**Hospital Database Management System**

The objective of the project is to computerize the office management of a Hospital leading to a simple,organized and easy to access system.
The Hospital Management System deals with the registration of patients and their medical records,keeps track of doctors,nurses and other technical and non-technical staff in the hospital. 
Information concerning admission of new patients, scheduling appointments and tests,bill payment etc., are also dealt with by this management system.

The system supports 6 different logins - admin,doctor,patient,nurse,receptionist and technical staff.
A few screens of the webapp are as shown:
![login page](./DBMS_project/images/login_page.png "Login Page")
![admin home page](./DBMS_project/images/admin_page.png "Admin Home Page")
![request appointment](./DBMS_project/images/req_apptmt.png "Appointment Request Page for Patient Login")
      
The project was completed using LAMP stack,with MariaDB as the database server.
The User Interface Design was done using HTML,CSS and JavaScript.
The database backend makes use of MySQL functions,procedures and views.
   
