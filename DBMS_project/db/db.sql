-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: Hospital
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB-1:10.5.9+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admission`
--

DROP TABLE IF EXISTS `admission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admission` (
  `patient_ID` varchar(50) NOT NULL,
  `appointment_ID` varchar(30) NOT NULL,
  `room_number` varchar(30) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `discharge_date` date DEFAULT NULL,
  PRIMARY KEY (`patient_ID`,`appointment_ID`),
  KEY `appointment_ID` (`appointment_ID`),
  KEY `room_number` (`room_number`),
  CONSTRAINT `admission_ibfk_1` FOREIGN KEY (`patient_ID`) REFERENCES `patient` (`patient_ID`),
  CONSTRAINT `admission_ibfk_2` FOREIGN KEY (`appointment_ID`) REFERENCES `appointment` (`appointment_ID`),
  CONSTRAINT `admission_ibfk_3` FOREIGN KEY (`room_number`) REFERENCES `room` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admission`
--

LOCK TABLES `admission` WRITE;
/*!40000 ALTER TABLE `admission` DISABLE KEYS */;
INSERT INTO `admission` VALUES ('PA0001','AP0001','103','2021-05-07',NULL),('PA0009','AP0005','103','2020-10-05','2021-05-05'),('PA0025','AP0013','102','2020-10-11','2020-10-15'),('PA0037','AP0020','111','2020-10-18','2020-10-22'),('PA0041','AP0023','101','2021-05-07',NULL),('PA0042','AP0024','113','2020-10-20','2020-10-22');
/*!40000 ALTER TABLE `admission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `appointment_ID` varchar(50) NOT NULL,
  `doctor_ID` varchar(50) DEFAULT NULL,
  `receptionist_ID` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time DEFAULT NULL,
  `status` varchar(20) DEFAULT 'Approved',
  PRIMARY KEY (`appointment_ID`),
  KEY `doctor_ID` (`doctor_ID`),
  KEY `receptionist_ID` (`receptionist_ID`),
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`doctor_ID`) REFERENCES `doctor` (`doctor_ID`),
  CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`receptionist_ID`) REFERENCES `receptionist` (`receptionist_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES ('AP0001','DR0001','RE001','2020-10-05','09:10:10','Cancel'),('AP0002','DR0001','RE001','2020-10-05','09:20:10','Approved'),('AP0003','DR0003','RE002','2020-10-05','09:25:10','Approved'),('AP0004','DR0021','RE003','2020-10-05','09:30:10','Approved'),('AP0005','DR0024','RE002','2020-10-05','10:30:10','Approved'),('AP0006','DR0019','RE002','2020-10-05','10:40:10','Approved'),('AP0007','DR0009','RE001','2020-10-06','10:32:10','Approved'),('AP0008','DR0009','RE004','2020-10-06','11:32:10','Approved'),('AP0009','DR0007','RE004','2020-10-06','12:32:10','Approved'),('AP0010','DR0007','RE002','2020-10-07','12:32:10','Approved'),('AP0011','DR0007','RE003','2020-10-09','15:32:10','Approved'),('AP0012','DR0021','RE001','2020-10-10','11:32:10','Approved'),('AP0013','DR0021','RE004','2020-10-11','16:32:10','Approved'),('AP0014','DR0014','RE001','2020-10-14','09:32:10','Approved'),('AP0015','DR0020','RE003','2020-10-16','11:32:10','Approved'),('AP0016','DR0020','RE001','2020-10-17','13:32:10','Approved'),('AP0017','DR0014','RE001','2020-10-17','15:32:10','Approved'),('AP0018','DR0001','RE002','2020-10-18','11:32:10','Approved'),('AP0019','DR0001','RE001','2020-10-18','13:32:10','Approved'),('AP0020','DR0003','RE003','2020-10-18','15:32:10','Approved'),('AP0021','DR0007','RE002','2020-10-19','11:32:10','Approved'),('AP0022','DR0009','RE004','2020-10-19','14:32:10','Approved'),('AP0023','DR0025','RE004','2020-10-20','10:32:10','Approved'),('AP0024','DR0025','RE001','2020-10-20','12:32:10','Approved'),('AP0025',NULL,'RE003','2020-05-01',NULL,'Pending'),('AP0026',NULL,'RE001','2020-05-01',NULL,'Pending'),('AP0027',NULL,'RE001','2020-05-01',NULL,'Pending'),('AP0028',NULL,'RE003','2021-04-29',NULL,'Pending'),('AP0029',NULL,'RE003','2021-04-29',NULL,'Pending'),('AP0030',NULL,'RE001','2020-05-01',NULL,'Pending'),('AP0031',NULL,'RE001','2020-05-01',NULL,'Pending'),('AP0032','DR0007','RE003','2021-05-07','08:00:00','Pending');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `appointment_orders`
--

DROP TABLE IF EXISTS `appointment_orders`;
/*!50001 DROP VIEW IF EXISTS `appointment_orders`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `appointment_orders` (
  `doctor_id` tinyint NOT NULL,
  `doctor_name` tinyint NOT NULL,
  `patient_id` tinyint NOT NULL,
  `patient_name` tinyint NOT NULL,
  `time` tinyint NOT NULL,
  `date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `assist`
--

DROP TABLE IF EXISTS `assist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assist` (
  `test_ID` varchar(50) NOT NULL,
  `tech_staff_ID` varchar(50) NOT NULL,
  PRIMARY KEY (`test_ID`,`tech_staff_ID`),
  KEY `tech_staff_ID` (`tech_staff_ID`),
  CONSTRAINT `assist_ibfk_1` FOREIGN KEY (`test_ID`) REFERENCES `test` (`test_ID`),
  CONSTRAINT `assist_ibfk_2` FOREIGN KEY (`tech_staff_ID`) REFERENCES `technical_staff` (`tech_staff_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assist`
--

LOCK TABLES `assist` WRITE;
/*!40000 ALTER TABLE `assist` DISABLE KEYS */;
INSERT INTO `assist` VALUES ('TS001','TE002'),('TS001','TE006'),('TS002','TE010'),('TS003','TE005'),('TS004','TE008'),('TS005','TE008'),('TS006','TE003'),('TS007','TE006');
/*!40000 ALTER TABLE `assist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `available_rooms`
--

DROP TABLE IF EXISTS `available_rooms`;
/*!50001 DROP VIEW IF EXISTS `available_rooms`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `available_rooms` (
  `room_number` tinyint NOT NULL,
  `nurse_ID` tinyint NOT NULL,
  `nurse_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `bills`
--

DROP TABLE IF EXISTS `bills`;
/*!50001 DROP VIEW IF EXISTS `bills`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `bills` (
  `patient_ID` tinyint NOT NULL,
  `appointment_ID` tinyint NOT NULL,
  `tot_med_cost` tinyint NOT NULL,
  `tot_test_cost` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `check_med_exp`
--

DROP TABLE IF EXISTS `check_med_exp`;
/*!50001 DROP VIEW IF EXISTS `check_med_exp`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `check_med_exp` (
  `medicine_ID` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `expiry` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dept`
--

DROP TABLE IF EXISTS `dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dept` (
  `department_ID` varchar(8) NOT NULL,
  `dept_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dept`
--

LOCK TABLES `dept` WRITE;
/*!40000 ALTER TABLE `dept` DISABLE KEYS */;
INSERT INTO `dept` VALUES ('CD1010','Cardiology'),('EN1201','ENT'),('GY1201','Gynecology'),('NE1110','Nephrology'),('OR1110','Orthopaedics'),('PM1201','Pulmonary'),('PS1011','Psychiatry');
/*!40000 ALTER TABLE `dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `doctor_ID` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `department_ID` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `designation` varchar(50) NOT NULL,
  `shift` varchar(50) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`doctor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES ('DR0001','Atif','CD1010','India','Kerala','Alappuzha','Rajeev Nagar','1986-10-21','MD Resident','Morning',60000),('DR0003','Arjit','PS1011','India','Kerala','Kochi','Indira Nagar','1987-11-21','MD Resident','Morning',70000),('DR0007','Shaan','EN1201','India','Kerala','Kollam','SM Street','1979-12-11','MD Resident','Afternoon',75000),('DR0009','Shreya','PM1201','India','Kerala','Kollam','SM Nagar','1977-12-21','MD Resident','Afternoon',70000),('DR0014','Rahman','OR1110','India','Kerala','Calicut','SM Street','1973-11-08','MD Resident','Night',80000),('DR0019','Mohit','PM1201','India','Kerala','Calicut','SM Street','1983-11-11','MD Resident','Night',50000),('DR0020','Jubin','NE1110','India','Kerala','Kochi','RP Street','1983-11-28','Chief Surgeon','Night',85000),('DR0021','Neha','GY1201','India','Kerala','Malappuram','RK Nagar','1976-10-29','Chief Surgeon','Morning',60000),('DR0024','Anirudh','PM1201','India','Kerala','Kochi','MKV Street','1972-10-12','Chief Surgeon','Morning',95000),('DR0025','Andrea','OR1110','India','Kerala','Kannur','RP Nagar','1978-10-19','Chief Surgeon','Morning',85000);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor_phone`
--

DROP TABLE IF EXISTS `doctor_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor_phone` (
  `doctor_ID` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`doctor_ID`,`phone_number`),
  CONSTRAINT `doctor_phone_ibfk_1` FOREIGN KEY (`doctor_ID`) REFERENCES `doctor` (`doctor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor_phone`
--

LOCK TABLES `doctor_phone` WRITE;
/*!40000 ALTER TABLE `doctor_phone` DISABLE KEYS */;
INSERT INTO `doctor_phone` VALUES ('DR0001',890890989),('DR0001',976343210),('DR0003',947648900),('DR0007',897348900),('DR0009',874754890),('DR0009',944758900),('DR0014',894751340),('DR0019',798951230),('DR0020',700851242),('DR0021',701234083),('DR0024',840399707),('DR0025',944757567);
/*!40000 ALTER TABLE `doctor_phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES ('doctor','doc123'),('admin','adm123'),('patient','pat123'),('nurse','nur123'),('receptionist','rec123'),('tech_staff','tec123');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maintains`
--

DROP TABLE IF EXISTS `maintains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maintains` (
  `record_number` varchar(50) NOT NULL,
  `receptionist_ID` varchar(50) NOT NULL,
  PRIMARY KEY (`record_number`),
  KEY `receptionist_ID` (`receptionist_ID`),
  CONSTRAINT `maintains_ibfk_1` FOREIGN KEY (`receptionist_ID`) REFERENCES `receptionist` (`receptionist_ID`),
  CONSTRAINT `maintains_ibfk_2` FOREIGN KEY (`record_number`) REFERENCES `medical_record` (`record_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maintains`
--

LOCK TABLES `maintains` WRITE;
/*!40000 ALTER TABLE `maintains` DISABLE KEYS */;
INSERT INTO `maintains` VALUES ('RN020','RE001'),('RN031','RE001'),('RN033','RE001'),('RN010','RE002'),('RN024','RE002'),('RN029','RE002'),('RN012','RE003'),('RN013','RE003'),('RN023','RE004'),('RN032','RE004');
/*!40000 ALTER TABLE `maintains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_record`
--

DROP TABLE IF EXISTS `medical_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medical_record` (
  `record_number` varchar(50) NOT NULL,
  `patient_ID` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`record_number`),
  KEY `patient_ID` (`patient_ID`),
  CONSTRAINT `medical_record_ibfk_1` FOREIGN KEY (`patient_ID`) REFERENCES `patient` (`patient_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_record`
--

LOCK TABLES `medical_record` WRITE;
/*!40000 ALTER TABLE `medical_record` DISABLE KEYS */;
INSERT INTO `medical_record` VALUES ('RN010','PA0003','Medical history : Stomach Ulcers, Treatments: Proton-pump inhibitor'),('RN012','PA0032','Medical history : Frequent Urination, Treatments: None'),('RN013','PA0001','Medical history : Diabetes, Treatments: Anti-diabetic medication'),('RN020','PA0014','Medical history : Kidney Stones, Treatments: Pain relievers'),('RN023','PA0040','Medical history : High blood pressure, Treatments: ACE inhibitor'),('RN024','PA0041','Medical history : Thyroid , Treatments : Blood test'),('RN029','PA0003','Medical history : Asthma, Treatments: Bronchodilator'),('RN031','PA0022','Medical history : Asthma, Treatments: Oxygen therapy'),('RN032','PA0003','Medical history : Heartburn, Treatments: Proton-pump inhibitor'),('RN033','PA0009','NULL');
/*!40000 ALTER TABLE `medical_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine` (
  `medicine_ID` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `cost` int(11) NOT NULL,
  `manufacturing` date NOT NULL,
  `expiry` date NOT NULL,
  PRIMARY KEY (`medicine_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` VALUES ('CD12001','Zinetac','Incepta','Antacid',15,'2021-02-10','2024-12-10'),('CD12021','Diazone','Cipla','Antibiotic',100,'2020-12-12','2024-12-12'),('EN13001','Yamadin','Cipla','Antibiotic',35,'2020-11-12','2023-10-10'),('GY14003','Napa','Glaxo','Steroid',50,'2021-03-12','2024-03-12'),('OR21002','Awakin','Cipla','Steroid',20,'2021-02-02','2024-02-02'),('PM11001','Omidon','Glaxo','Antibiotic',30,'2020-12-12','2022-12-12'),('PM11003','Zymet','Glaxo','Antibiotic',40,'2020-12-22','2023-11-12'),('PS15003','Xanax','Glaxo','Sedative',70,'2021-03-01','2023-03-01');
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `medicines_sold`
--

DROP TABLE IF EXISTS `medicines_sold`;
/*!50001 DROP VIEW IF EXISTS `medicines_sold`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `medicines_sold` (
  `medicine_id` tinyint NOT NULL,
  `company` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `med_sold` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `nurse`
--

DROP TABLE IF EXISTS `nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse` (
  `nurse_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `shift` varchar(50) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`nurse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nurse`
--

LOCK TABLES `nurse` WRITE;
/*!40000 ALTER TABLE `nurse` DISABLE KEYS */;
INSERT INTO `nurse` VALUES ('NU002','Shilpa','India','Kerala','Kottayam','Gandhi Nagar','1992-10-11','Night',15000),('NU005','Asha','India','Kerala','Kannur','Mittai Theriv','1992-08-25','Night',15000),('NU006','Antony','India','Kerala','Idukki','Balan City','1989-02-21','Morning',16000),('NU007','Grace','India','Kerala','Kollam','Chavara','1990-01-21','Afternoon',16000),('NU009','Elsa','India','Kerala','Kannur','Mittai Theriv','1979-12-29','Morning',17000),('NU011','Meera','India','Kerala','Kochi','Marine Drive','1985-07-06','Night',16500),('NU013','Jerry','India','Kerala','Kollam','Karunagappally','1990-08-21','Afternoon',16000),('NU017','Michael','India','Kerala','Pathanamthitta','College Road','1971-08-21','Night',18000),('NU019','Ashiq','India','Kerala','Thrissur','Maidanam','1985-01-01','Afternoon',17500);
/*!40000 ALTER TABLE `nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nurse_phone`
--

DROP TABLE IF EXISTS `nurse_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse_phone` (
  `nurse_id` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`nurse_id`,`phone_number`),
  CONSTRAINT `nurse_phone_ibfk_1` FOREIGN KEY (`nurse_id`) REFERENCES `nurse` (`nurse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nurse_phone`
--

LOCK TABLES `nurse_phone` WRITE;
/*!40000 ALTER TABLE `nurse_phone` DISABLE KEYS */;
INSERT INTO `nurse_phone` VALUES ('NU002',942658876),('NU005',925644666),('NU005',994754856),('NU006',846799541),('NU007',745869541),('NU009',945785671),('NU011',945785246),('NU013',745865246),('NU017',875663246),('NU019',678444856),('NU019',969853188);
/*!40000 ALTER TABLE `nurse_phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nursing_service`
--

DROP TABLE IF EXISTS `nursing_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_service` (
  `room_number` varchar(50) NOT NULL,
  `nurse_id` varchar(50) NOT NULL,
  PRIMARY KEY (`room_number`),
  KEY `nurse_id` (`nurse_id`),
  CONSTRAINT `nursing_service_ibfk_1` FOREIGN KEY (`nurse_id`) REFERENCES `nurse` (`nurse_id`),
  CONSTRAINT `nursing_service_ibfk_2` FOREIGN KEY (`room_number`) REFERENCES `room` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nursing_service`
--

LOCK TABLES `nursing_service` WRITE;
/*!40000 ALTER TABLE `nursing_service` DISABLE KEYS */;
INSERT INTO `nursing_service` VALUES ('102','NU005'),('103','NU006'),('101','NU007'),('111','NU009'),('112','NU011'),('113','NU017');
/*!40000 ALTER TABLE `nursing_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `patient_ID` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  PRIMARY KEY (`patient_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES ('PA0001','Aarav','India','Kerala','Kannur','AP Nagar','Male','1990-11-11'),('PA0002','Aarnav','India','Kerala','Kochi','KR Street','Male','1980-12-12'),('PA0003','Moni','India','Kerala','Palakkad','Ahalia Road','Female','1982-11-10'),('PA0004','Priya','India','Kerala','Alleppey','AK Nagar','Female','1972-12-11'),('PA0009','Aneesh','India','Kerala','Kollam','CT Road','Male','1992-03-03'),('PA0010','Sindhu','India','Kerala','Palakkad','AR Street','Female','1972-02-02'),('PA0013','Bindu','India','Kerala','Guruvayur','RT Nagar','Female','1990-08-08'),('PA0014','Roohi','India','Kerala','Guruvayur','CR Street','Female','1990-11-08'),('PA0015','Anand','India','Tamil Nadu','Coimbatore','Anna Road','Male','1972-03-08'),('PA0018','Krishna','India','Kerala','Kochi','MG Road','Male','1982-03-11'),('PA0020','Raju','India','Kerala','Kochi','SK Town','Male','1982-04-01'),('PA0022','Hima','India','Kerala','Palakkad','AJ Road','Female','1992-04-15'),('PA0025','Padma','India','Kerala','Alleppey','SR town','Female','1982-04-19'),('PA0027','Latha','India','Kerala','Munnar','MK Nagar','Female','1982-04-12'),('PA0028','Venu','India','Kerala','Thrissur','GR Street','Male','1992-09-11'),('PA0030','Gopal','India','Kerala','Thrissur','GR Street','Male','1980-10-10'),('PA0032','Haritha','India','Kerala','Trivandrum','Temple Road','Female','1995-11-10'),('PA0034','Kumar','India','Kerala','Trivandrum','HG Road','Male','1985-12-10'),('PA0036','Poorna','India','Kerala','Munnar','HT Town','Female','1975-05-03'),('PA0037','Ratna','India','Tamil Nadu','Coimbatore','AT Street','Female','1972-05-09'),('PA0038','Paul','India','Kerala','Trivandrum','Temple Town','Male','1980-05-11'),('PA0040','Jessie','India','Kerala','Kochi','ST Street','Female','1995-04-12'),('PA0041','Renu','India','Kerala','Kochi','ST Town','Female','1992-03-15'),('PA0042','Kanna','India','Kerala','Kollam','RD Colony','Male','1982-04-15'),('PA0043','Aadya','India','Telangana','Hyderabad','122','Female','2000-05-01'),('PA0056','Sara','India','Telangana','Hyderabad','141','Female','2001-05-30');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `patient_medical_details`
--

DROP TABLE IF EXISTS `patient_medical_details`;
/*!50001 DROP VIEW IF EXISTS `patient_medical_details`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `patient_medical_details` (
  `patient_ID` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `gender` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `age` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `patient_phone`
--

DROP TABLE IF EXISTS `patient_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_phone` (
  `patient_ID` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`patient_ID`,`phone_number`),
  CONSTRAINT `patient_phone_ibfk_1` FOREIGN KEY (`patient_ID`) REFERENCES `patient` (`patient_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_phone`
--

LOCK TABLES `patient_phone` WRITE;
/*!40000 ALTER TABLE `patient_phone` DISABLE KEYS */;
INSERT INTO `patient_phone` VALUES ('PA0001',987654321),('PA0002',987765679),('PA0003',876897997),('PA0004',879876765),('PA0009',987876567),('PA0010',987656789),('PA0013',789765567),('PA0014',897678999),('PA0015',897789678),('PA0018',789876555),('PA0020',789999678),('PA0022',908908908),('PA0025',890890890),('PA0027',908987678),('PA0028',908768909),('PA0030',890789078),('PA0032',999898787),('PA0034',998878989),('PA0036',909080789),('PA0037',890876543),('PA0038',987656789),('PA0040',876543299),('PA0041',789876567),('PA0042',879999987);
/*!40000 ALTER TABLE `patient_phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `payment_ID` varchar(8) NOT NULL,
  `receptionist_ID` varchar(8) DEFAULT NULL,
  `patient_ID` varchar(8) DEFAULT NULL,
  `appointment_ID` varchar(10) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `date` date DEFAULT NULL,
  `mode_of_payment` varchar(15) DEFAULT NULL,
  `payment_status` varchar(10) DEFAULT 'PAID',
  PRIMARY KEY (`payment_ID`),
  KEY `receptionist_ID` (`receptionist_ID`),
  KEY `patient_ID` (`patient_ID`),
  KEY `appointment_ID` (`appointment_ID`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`receptionist_ID`) REFERENCES `receptionist` (`receptionist_ID`),
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`patient_ID`) REFERENCES `patient` (`patient_ID`),
  CONSTRAINT `payment_ibfk_3` FOREIGN KEY (`appointment_ID`) REFERENCES `appointment` (`appointment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES ('BL12010',NULL,'PA0025','AP0013',4800,NULL,NULL,'NOT PAID'),('BL12011','RE002','PA0018','AP0010',2500,'2020-10-05','Cash','PAID'),('BL12012','RE003','PA0009','AP0005',2100,'2020-10-05','Credit','PAID'),('BL12013','RE004','PA0042','AP0024',1900,'2020-10-20','Cash','PAID'),('BL12014','RE003','PA0032','AP0017',2000,'2020-10-17','Credit','PAID');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescribes`
--

DROP TABLE IF EXISTS `prescribes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescribes` (
  `medicine_ID` varchar(50) NOT NULL,
  `doctor_ID` varchar(50) NOT NULL,
  `appointment_ID` varchar(50) NOT NULL,
  PRIMARY KEY (`medicine_ID`,`doctor_ID`,`appointment_ID`),
  KEY `doctor_ID` (`doctor_ID`),
  KEY `appointment_ID` (`appointment_ID`),
  CONSTRAINT `prescribes_ibfk_1` FOREIGN KEY (`medicine_ID`) REFERENCES `medicine` (`medicine_ID`),
  CONSTRAINT `prescribes_ibfk_2` FOREIGN KEY (`doctor_ID`) REFERENCES `doctor` (`doctor_ID`),
  CONSTRAINT `prescribes_ibfk_3` FOREIGN KEY (`appointment_ID`) REFERENCES `appointment` (`appointment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescribes`
--

LOCK TABLES `prescribes` WRITE;
/*!40000 ALTER TABLE `prescribes` DISABLE KEYS */;
INSERT INTO `prescribes` VALUES ('CD12001','DR0001','AP0004'),('CD12001','DR0021','AP0015'),('CD12021','DR0001','AP0004'),('EN13001','DR0021','AP0015'),('GY14003','DR0021','AP0015'),('PM11003','DR0007','AP0023'),('PS15003','DR0009','AP0009');
/*!40000 ALTER TABLE `prescribes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receptionist`
--

DROP TABLE IF EXISTS `receptionist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receptionist` (
  `receptionist_ID` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`receptionist_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receptionist`
--

LOCK TABLES `receptionist` WRITE;
/*!40000 ALTER TABLE `receptionist` DISABLE KEYS */;
INSERT INTO `receptionist` VALUES ('RE001','Sara','India','Kerala','Kochi','Teepeyem Enclave, Kaloor Rd','1996-01-01',10000),('RE002','Ishwar','India','Kerala','Trivandrum','14-5 Gandhi Rd','1995-05-23',10000),('RE003','Aiesha','India','Kerala','Kochi','31-2, Infopark Rd','1995-10-15',10000),('RE004','Manoj','India','Kerala','Kochi','24-5 Jew Street','1997-02-17',10000);
/*!40000 ALTER TABLE `receptionist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receptionist_phone`
--

DROP TABLE IF EXISTS `receptionist_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receptionist_phone` (
  `receptionist_ID` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`receptionist_ID`,`phone_number`),
  CONSTRAINT `receptionist_phone_ibfk_1` FOREIGN KEY (`receptionist_ID`) REFERENCES `receptionist` (`receptionist_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receptionist_phone`
--

LOCK TABLES `receptionist_phone` WRITE;
/*!40000 ALTER TABLE `receptionist_phone` DISABLE KEYS */;
INSERT INTO `receptionist_phone` VALUES ('RE001',900034678),('RE002',899945673),('RE003',987654321),('RE004',998765433);
/*!40000 ALTER TABLE `receptionist_phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `test_ID` varchar(50) NOT NULL,
  `patient_ID` varchar(50) NOT NULL,
  `doctor_ID` varchar(50) NOT NULL,
  PRIMARY KEY (`test_ID`,`patient_ID`),
  KEY `patient_ID` (`patient_ID`),
  KEY `doctor_ID` (`doctor_ID`),
  CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`patient_ID`) REFERENCES `patient` (`patient_ID`),
  CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`doctor_ID`) REFERENCES `doctor` (`doctor_ID`),
  CONSTRAINT `requests_ibfk_3` FOREIGN KEY (`test_ID`) REFERENCES `test` (`test_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES ('TS001','PA0042','DR0007'),('TS003','PA0041','DR0007'),('TS002','PA0018','DR0009'),('TS004','PA0037','DR0009'),('TS008','PA0004','DR0014'),('TS007','PA0003','DR0020'),('TS006','PA0032','DR0024'),('TS007','PA0025','DR0024'),('TS006','PA0018','DR0025');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `room_number` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES ('101','Not available '),('102','Not available'),('103','Available'),('111','Not available'),('112','Available'),('113','Not available');
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `takes`
--

DROP TABLE IF EXISTS `takes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `takes` (
  `appointment_ID` varchar(50) NOT NULL,
  `patient_ID` varchar(50) NOT NULL,
  PRIMARY KEY (`appointment_ID`),
  KEY `patient_ID` (`patient_ID`),
  CONSTRAINT `takes_ibfk_1` FOREIGN KEY (`patient_ID`) REFERENCES `patient` (`patient_ID`),
  CONSTRAINT `takes_ibfk_2` FOREIGN KEY (`appointment_ID`) REFERENCES `appointment` (`appointment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `takes`
--

LOCK TABLES `takes` WRITE;
/*!40000 ALTER TABLE `takes` DISABLE KEYS */;
INSERT INTO `takes` VALUES ('AP0001','PA0001'),('AP0025','PA0001'),('AP0026','PA0001'),('AP0027','PA0001'),('AP0028','PA0001'),('AP0029','PA0001'),('AP0030','PA0001'),('AP0031','PA0001'),('AP0032','PA0001'),('AP0002','PA0002'),('AP0003','PA0003'),('AP0004','PA0004'),('AP0005','PA0009'),('AP0006','PA0010'),('AP0007','PA0013'),('AP0008','PA0014'),('AP0009','PA0015'),('AP0010','PA0018'),('AP0011','PA0020'),('AP0012','PA0022'),('AP0013','PA0025'),('AP0014','PA0027'),('AP0015','PA0028'),('AP0016','PA0030'),('AP0017','PA0032'),('AP0018','PA0034'),('AP0019','PA0036'),('AP0020','PA0037'),('AP0021','PA0038'),('AP0022','PA0040'),('AP0023','PA0041'),('AP0024','PA0042');
/*!40000 ALTER TABLE `takes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tech_phone`
--

DROP TABLE IF EXISTS `tech_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tech_phone` (
  `tech_staff_ID` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`tech_staff_ID`,`phone_number`),
  CONSTRAINT `tech_phone_ibfk_1` FOREIGN KEY (`tech_staff_ID`) REFERENCES `technical_staff` (`tech_staff_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tech_phone`
--

LOCK TABLES `tech_phone` WRITE;
/*!40000 ALTER TABLE `tech_phone` DISABLE KEYS */;
INSERT INTO `tech_phone` VALUES ('TE002',674587571),('TE003',978587571),('TE005',978547585),('TE006',745862148),('TE006',978458624),('TE008',745867568),('TE010',614521769);
/*!40000 ALTER TABLE `tech_phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technical_staff`
--

DROP TABLE IF EXISTS `technical_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `technical_staff` (
  `tech_staff_ID` varchar(50) NOT NULL,
  `lab_ID` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `date_of_birth` date NOT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`tech_staff_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technical_staff`
--

LOCK TABLES `technical_staff` WRITE;
/*!40000 ALTER TABLE `technical_staff` DISABLE KEYS */;
INSERT INTO `technical_staff` VALUES ('TE002','LB01','Aswin','India','Kerala','Trivandrum','Kovalam','1989-02-01',14500),('TE003','LB02','Abraham','India','Kerala','Calicut','Vamanapuram','1992-07-06',15600),('TE005','LB02','Sreeraj','India','Kerala','Thrissur','Maidanam','1989-02-01',14500),('TE006','LB03','Vivek','India','Kerala','Calicut','Koyilandi','1986-11-21',16100),('TE008','LB04','Maria','India','Kerala','Thrissur','Athirappally','1991-08-06',15900),('TE010','LB01','Amala','India','Kerala','Wayanad','Churam','1986-01-22',16100);
/*!40000 ALTER TABLE `technical_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `test_ID` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `test_details` varchar(100) DEFAULT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`test_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES ('TS001','Blood Test','Test result-12 hrs',500),('TS002','Urine Test','Test result-12 hrs',500),('TS003','X-ray','Test result-24 hrs',450),('TS004','CT Scan','Test result-24 hrs',450),('TS005','MRI Scan','Test result-24 hrs',450),('TS006','Endoscopy','Test result-72 hrs',2000),('TS007','Biopsy','Test result-72 hrs',2000),('TS008','Full medical checkup','Test result-72 hrs',2500);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `tot_cost`
--

DROP TABLE IF EXISTS `tot_cost`;
/*!50001 DROP VIEW IF EXISTS `tot_cost`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tot_cost` (
  `patient_ID` tinyint NOT NULL,
  `appointment_ID` tinyint NOT NULL,
  `tot_med_cost` tinyint NOT NULL,
  `tot_test_cost` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `appointment_orders`
--

/*!50001 DROP TABLE IF EXISTS `appointment_orders`*/;
/*!50001 DROP VIEW IF EXISTS `appointment_orders`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `appointment_orders` AS select `doctor`.`doctor_ID` AS `doctor_id`,`doctor`.`name` AS `doctor_name`,`patient`.`patient_ID` AS `patient_id`,`patient`.`name` AS `patient_name`,`appointment`.`time` AS `time`,`appointment`.`date` AS `date` from (((`patient` join `takes` on(`patient`.`patient_ID` = `takes`.`patient_ID`)) join `appointment` on(`takes`.`appointment_ID` = `appointment`.`appointment_ID`)) join `doctor` on(`appointment`.`doctor_ID` = `doctor`.`doctor_ID`)) order by `doctor`.`doctor_ID`,`appointment`.`date`,`appointment`.`time` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `available_rooms`
--

/*!50001 DROP TABLE IF EXISTS `available_rooms`*/;
/*!50001 DROP VIEW IF EXISTS `available_rooms`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `available_rooms` AS select `room`.`room_number` AS `room_number`,`nursing_service`.`nurse_id` AS `nurse_ID`,`nurse`.`name` AS `nurse_name` from ((`room` join `nursing_service` on(`room`.`room_number` = `nursing_service`.`room_number`)) join `nurse` on(`nursing_service`.`nurse_id` = `nurse`.`nurse_id`)) where `room`.`status` = 'Available' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `bills`
--

/*!50001 DROP TABLE IF EXISTS `bills`*/;
/*!50001 DROP VIEW IF EXISTS `bills`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bills` AS with x as (select `Hospital`.`takes`.`patient_ID` AS `patient_ID`,`Hospital`.`takes`.`appointment_ID` AS `appointment_ID`,`temp1`.`med_cost` AS `tot_med_cost` from (`Hospital`.`takes` join (select `Hospital`.`prescribes`.`appointment_ID` AS `appointment_ID`,sum(`Hospital`.`medicine`.`cost`) AS `med_cost` from (`Hospital`.`medicine` join `Hospital`.`prescribes` on(`Hospital`.`medicine`.`medicine_ID` = `Hospital`.`prescribes`.`medicine_ID`)) group by `Hospital`.`prescribes`.`appointment_ID`) `temp1` on(`Hospital`.`takes`.`appointment_ID` = `temp1`.`appointment_ID`))), y as (select `Hospital`.`takes`.`patient_ID` AS `patient_ID`,`Hospital`.`takes`.`appointment_ID` AS `appointment_ID`,`temp2`.`test_cost` AS `tot_test_cost` from (`Hospital`.`takes` join (select `Hospital`.`requests`.`patient_ID` AS `patient_ID`,sum(`Hospital`.`test`.`cost`) AS `test_cost` from (`Hospital`.`test` join `Hospital`.`requests` on(`Hospital`.`test`.`test_ID` = `Hospital`.`requests`.`test_ID`)) group by `Hospital`.`requests`.`patient_ID`) `temp2` on(`Hospital`.`takes`.`patient_ID` = `temp2`.`patient_ID`)))(select `y`.`patient_ID` AS `patient_ID`,`y`.`appointment_ID` AS `appointment_ID`,ifnull(`x`.`tot_med_cost`,0) AS `tot_med_cost`,ifnull(`y`.`tot_test_cost`,0) AS `tot_test_cost` from (`y` left join `x` on(`x`.`patient_ID` = `y`.`patient_ID` and `x`.`appointment_ID` = `y`.`appointment_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `check_med_exp`
--

/*!50001 DROP TABLE IF EXISTS `check_med_exp`*/;
/*!50001 DROP VIEW IF EXISTS `check_med_exp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `check_med_exp` AS select `medicine`.`medicine_ID` AS `medicine_ID`,`medicine`.`name` AS `name`,`medicine`.`expiry` AS `expiry` from `medicine` where to_days(curdate()) - to_days(`medicine`.`expiry`) > 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `medicines_sold`
--

/*!50001 DROP TABLE IF EXISTS `medicines_sold`*/;
/*!50001 DROP VIEW IF EXISTS `medicines_sold`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `medicines_sold` AS select `medicine`.`medicine_ID` AS `medicine_id`,`medicine`.`company` AS `company`,`medicine`.`name` AS `name`,count(`medicine`.`name`) AS `med_sold` from (`medicine` join `prescribes` on(`medicine`.`medicine_ID` = `prescribes`.`medicine_ID`)) group by `medicine`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `patient_medical_details`
--

/*!50001 DROP TABLE IF EXISTS `patient_medical_details`*/;
/*!50001 DROP VIEW IF EXISTS `patient_medical_details`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `patient_medical_details` AS select `patient`.`patient_ID` AS `patient_ID`,`patient`.`name` AS `name`,`patient`.`gender` AS `gender`,`medical_record`.`description` AS `description`,year(curdate()) - year(`patient`.`date_of_birth`) AS `age` from (`patient` left join `medical_record` on(`patient`.`patient_ID` = `medical_record`.`patient_ID`)) order by `patient`.`patient_ID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tot_cost`
--

/*!50001 DROP TABLE IF EXISTS `tot_cost`*/;
/*!50001 DROP VIEW IF EXISTS `tot_cost`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tot_cost` AS (select `Hospital`.`takes`.`patient_ID` AS `patient_ID`,`Hospital`.`takes`.`appointment_ID` AS `appointment_ID`,`temp1`.`med_cost` AS `tot_med_cost`,NULL AS `tot_test_cost` from (`Hospital`.`takes` join (select `Hospital`.`prescribes`.`appointment_ID` AS `appointment_ID`,sum(`Hospital`.`medicine`.`cost`) AS `med_cost` from (`Hospital`.`medicine` join `Hospital`.`prescribes` on(`Hospital`.`medicine`.`medicine_ID` = `Hospital`.`prescribes`.`medicine_ID`)) group by `Hospital`.`prescribes`.`appointment_ID`) `temp1` on(`Hospital`.`takes`.`appointment_ID` = `temp1`.`appointment_ID`))) union (select `Hospital`.`takes`.`patient_ID` AS `patient_ID`,`Hospital`.`takes`.`appointment_ID` AS `appointment_ID`,NULL AS `NULL`,`y`.`test_cost` AS `test_cost` from (`Hospital`.`takes` join (select `Hospital`.`requests`.`patient_ID` AS `patient_ID`,sum(`Hospital`.`test`.`cost`) AS `test_cost` from (`Hospital`.`test` join `Hospital`.`requests` on(`Hospital`.`test`.`test_ID` = `Hospital`.`requests`.`test_ID`)) group by `Hospital`.`requests`.`patient_ID`) `y` on(`Hospital`.`takes`.`patient_ID` = `y`.`patient_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-07 14:31:51
