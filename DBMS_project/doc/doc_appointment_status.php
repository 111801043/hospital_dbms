<?php include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Update Appointment Status</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../doctor.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Doctor</a>
</li>
<li class="breadcrumb-item"><a href="view_user.php">Update Appointment Status</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Pending Appointment IDs</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="app_id" placeholder="Enter appointment ID...." required="" >
        </div>

        <label class="col-sm-2 col-form-label">Update Status</label>
        <div class="col-sm-4">
            <select class="form-control show-tick" name="status" >
                <option value="">Select</option>
                <?php
            $arr = array("Approved","Cancelled");
            foreach($arr as $val)
            {
                if($val == $rsedit['bloodgroup'])
                {
                    echo "<option value='$val' selected>$val</option>";
                }
                else
                {
                    echo "<option value='$val'>$val</option>";            
                }
            }
            ?>
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
if(isset($_POST['btn_submit'])){
	$stat = $_POST['status'];
	$id = $_POST['app_id'];
	$q = "update appointment set status = '$stat' where appointment_id = '$id' ";
	$a = $conn->query($q);
}

$conn->close();

?>