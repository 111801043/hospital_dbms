<?php include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Prescribe Medicine</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../doctor.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Doctor</a>
</li>
<li class="breadcrumb-item"><a href="view_user.php">Prescribe Medicine</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Appointment ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="app_id" placeholder="Enter appointment ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Medicine Name</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="med_name" placeholder="Enter medicine name...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Doctor ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="doc_id" placeholder="Enter doctor ID...." required="" >
        </div>

            </select>
        </div>

    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
$med_n = $_POST['med_name'];
$query = "SELECT medicine_ID,name FROM medicine where name = ('$med_n')";
$answer = $conn->query($query);

while ($row= mysqli_fetch_array($answer)){

        $i = $row["medicine_ID"];
}
?>
<?php

if(isset($_POST['btn_submit'])){
        $app_id = $_POST['app_id'];
	$d_id = $_POST['doc_id'];
        $q = "INSERT INTO prescribes(medicine_ID,doctor_ID,appointment_ID) VALUES ('$i','$d_id','$app_id')";
	if(mysqli_query($conn, $q)){
			$_SESSION['message'] = "Record has been updated";
}
}

$conn->close();
?>