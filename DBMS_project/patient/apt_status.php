<?php
  session_start();
  include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>View Appointment Status</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../patient.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Patient</a>
</li>
<li class="breadcrumb-item"><a href="profile.php">View Appointment Status</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<thead>
  <tr>
    <th>Appointment_ID</th>
    <th>Doctor_ID</th>
    <th>Doctor_name</th>
    <th>Date</th>
    <th>Time</th>
</tr>
</thead>
<tbody>
  <?php
    $userid = $_SESSION['userid'];
    $query = "SELECT appointment_ID, doctor_ID, name, date, time from takes natural join appointment natural join doctor where patient_ID = '$userid'";
    $qsql = $conn->query($query);
  while($rs = mysqli_fetch_array($qsql))
  {
  
    echo "<tr>
    <td>&nbsp;$rs[appointment_ID]</td>
    <td>&nbsp;$rs[doctor_ID]</td>
    <td>&nbsp;$rs[name]</td>
    <td>&nbsp;$rs[date]</td>
    <td>&nbsp;$rs[time]</td>
    </tr>";
  }        
?>
</tbody>


</table>
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>


