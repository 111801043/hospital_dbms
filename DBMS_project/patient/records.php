<?php
  session_start();
  include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>View Medical Record</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../patient.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Patient</a>
</li>
<li class="breadcrumb-item"><a href="records.php">View Medical Record</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<thead>
  <tr>
    <th>Description</th>
</tr>
</thead>
<tbody>
  <?php
    $userid = $_SESSION['userid'];
    $query = "SELECT description from medical_record where patient_ID = '$userid'";
    $qsql = $conn->query($query);
    if ($qsql->num_rows > 0) {
  while($rs = mysqli_fetch_array($qsql))
  {
  
    echo "<tr>
    <td>&nbsp;$rs[description]</td>
    </tr>";
  }        }
  else{
  echo "<tr>
    <td>&nbsp;No Result</td>
    </tr>";
  }
?>
</tbody>


</table>
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>


