<?php require_once('login_check.php');?>
<?php include('connect.php');?>

<!DOCTYPE html>
<html lang="english">
        <head>
        <title>Receptionist Info</title>
        <link rel="stylesheet" type="text/css" href="doc.css">
        <body background = "https://image.freepik.com/free-photo/doctor-s-stethoscope-with-blue-background_23-2147652363.jpg">
</head>
    </head>
    <body>
        <h4> Welcome Receptionist!! </h4>
        <div class = "doc">
           <ul>
                <li>
                    <a href="receptionist/make_appointments.php"><div class="box">Make Appointments</div></a>
                </li>
		<li> 
                    <a href="receptionist/view_appointments.php"><div class="box">View Appointments</div></a>
                </li>
                <li> 
                    <a href="receptionist/view_medical_records.php"><div class="box">View Medical Records</div></a>
                </li>    
                <li> 
                    <a href="receptionist/create_medical_records.php"><div class="box">Create Medical Records</div></a>
                </li>
                <li> 
                    <a href="receptionist/view_payments.php"><div class="box">View Payments</div></a>
                </li>
                <li> 
                    <a href="receptionist/room_checkout.php"><div class="box">Room Checkout</div></a>
                </li>
		<li> 
                    <a href="receptionist/patient_reg.php"><div class="box">Patient Registration</div></a>
                </li>
           </ul>   
        </div>
    </body>
</html>