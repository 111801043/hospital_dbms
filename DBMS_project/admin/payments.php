<?php include('head.php');?>
<?php include('connect.php');
if(isset($_GET['id']))
{
  $sql ="DELETE FROM payment WHERE payment_ID='$_GET[id]'";
  $qsql=mysqli_query($conn,$sql);
  if(mysqli_affected_rows($conn) == 1)
  {
    ?>
         <div class="popup popup--icon -success js_success-popup popup--visible">
          <div class="popup__background"></div>
          <div class="popup__content">
            <h3 class="popup__content__title">
              Success 
            </h3>
            <p>Payment record deleted successfully.</p>
            <p>
             <!--  <a href="index.php"><button class="button button--success" data-for="js_success-popup"></button></a> -->
             <?php echo "<script>setTimeout(\"location.href = 'payments.php';\",1500);</script>"; ?>
            </p>
          </div>
        </div>
<?php
    //echo "<script>alert('Dcctor record deleted successfully..');</script>";
    //echo "<script>window.location='view_doctor.php';</script>";
  }
}
?>

<?php
if(isset($_GET['delid']))
{ ?>
<div class="popup popup--icon -question js_question-popup popup--visible">
  <div class="popup__background"></div>
  <div class="popup__content">
    <h3 class="popup__content__title">
      Sure
    </h1>
    <p>Are You Sure To Delete This Record?</p>
    <p>
      <a href="payments.php?id=<?php echo $_GET['delid']; ?>" class="button button--success" data-for="js_success-popup">Yes</a>
      <a href="payments.php" class="button button--error" data-for="js_success-popup">No</a>
    </p>
  </div>
</div>
<?php } ?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Admin</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../admin.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Admin</a>
</li>
<li class="breadcrumb-item"><a href="payments.php">Payments</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>


<?php
if(isset($_GET['add_id'])){
?>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Payment ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="pay_id" placeholder="Enter payment ID...." required="" >
        </div>

        <label class="col-sm-2 col-form-label">Receptionist ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="rec_id" placeholder="Enter receptionist ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Patient ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="pat_id" placeholder="Enter patient ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Appointment ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="app_id" placeholder="Enter appointment ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Amount</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="amo" placeholder="Enter amount...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Date</label>
        <div class="col-sm-4">
            <input type="date" class="form-control" name="dat" placeholder="Enter date...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Mode of Payment</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="mod" placeholder="Enter mode of payment...." required="" >
        </div>
	
    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
if(isset($_POST['btn_submit'])){
	$pay_id = $_POST['pay_id'];
	$rec_id = $_POST['rec_id'];
	$pat_id = $_POST['pat_id'];
	$app_id = $_POST['app_id'];	
	$amo = $_POST['amo'];
	$dat = $_POST['dat'];
	$mod = $_POST['mod'];

	$q = "insert into payment values ('$pay_id','$rec_id','$pat_id','$app_id','$amo','$dat','$mod')";
	$a = $conn->query($q);
}

?>

<?php } ?>


<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<thead>
<tr>
    <th>Payment ID</th>
    <th>Receptionist ID</th>
    <th>Patient ID</th>
    <th>Appointment ID</th>
    <th>Amount</th>
    <th>Date</th>
    <th>Mode_Of_Payment</th>
    <th>Status</th>
</tr>
</thead>
<tbody>
<?php
  $sql ="SELECT * FROM payment";
  $qsql = mysqli_query($conn,$sql);
  while($rs = mysqli_fetch_array($qsql))
  {
    echo "<tr>
    <td>&nbsp;$rs[payment_ID]</td>
    <td>&nbsp;$rs[receptionist_ID]</td>
    <td>&nbsp;$rs[patient_ID]</td>
    <td>&nbsp;$rs[appointment_ID]</td>
    <td>&nbsp;$rs[amount]</td>
    <td>&nbsp;$rs[date]</td>
    <td>&nbsp;$rs[mode_of_payment]</td>
    <td>&nbsp;$rs[payment_status]</td>
    <td>&nbsp;
    <a href='payments.php?delid=$rs[payment_ID]' class='btn btn-danger'>Delete</a> </td>
    </tr>";
  }        
?>
</tbody>


</table>
 <a href='payments.php?add_id=$rs[payment_ID]' class='btn btn-danger'>Add payments</a> 
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>


