<?php include('head.php');?>
<?php include('connect.php');
if(isset($_GET['id']))
{
  $sql ="DELETE FROM doctor WHERE doctor_ID='$_GET[id]'";
  $qsql=mysqli_query($conn,$sql);
  if(mysqli_affected_rows($conn) == 1)
  {
    ?>
         <div class="popup popup--icon -success js_success-popup popup--visible">
          <div class="popup__background"></div>
          <div class="popup__content">
            <h3 class="popup__content__title">
              Success 
            </h3>
            <p>Doctor record deleted successfully.</p>
            <p>
             <!--  <a href="index.php"><button class="button button--success" data-for="js_success-popup"></button></a> -->
             <?php echo "<script>setTimeout(\"location.href = 'view_doctor.php';\",1500);</script>"; ?>
            </p>
          </div>
        </div>
<?php
    //echo "<script>alert('Dcctor record deleted successfully..');</script>";
    //echo "<script>window.location='view_doctor.php';</script>";
  }
}
?>

<?php
if(isset($_GET['delid']))
{ ?>
<div class="popup popup--icon -question js_question-popup popup--visible">
  <div class="popup__background"></div>
  <div class="popup__content">
    <h3 class="popup__content__title">
      Sure
    </h1>
    <p>Are You Sure To Delete This Record?</p>
    <p>
      <a href="view_doctor.php?id=<?php echo $_GET['delid']; ?>" class="button button--success" data-for="js_success-popup">Yes</a>
      <a href="view_doctor.php" class="button button--error" data-for="js_success-popup">No</a>
    </p>
  </div>
</div>
<?php } ?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Doctor</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../admin.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Admin</a>
</li>
<li class="breadcrumb-item"><a href="doctors.php">Doctor</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>


<?php
if(isset($_GET['add_id'])){
?>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Doctor ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="doc_id" placeholder="Enter doctor ID...." required="" >
        </div>

        <label class="col-sm-2 col-form-label">Doctor Name</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="doc_nam" placeholder="Enter doctor name...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Department</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="dep" placeholder="Enter department...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Country</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="cou" placeholder="Enter country...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">State</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="sta" placeholder="Enter state...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">City</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="cit" placeholder="Enter city...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Street</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="str" placeholder="Enter street...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Date of Birth</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="dob" placeholder="Enter DOB...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Designation</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="des" placeholder="Enter designation...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Shift</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="shi" placeholder="Enter shift...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Salary</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="sal" placeholder="Enter salary...." required="" >
        </div>

    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
if(isset($_POST['btn_submit'])){
	$doc_id = $_POST['doc_id'];
	$nam = $_POST['nam'];
	$dep_id = $_POST['dep_id'];
	$cou = $_POST['cou'];	
	$sta = $_POST['sta'];
	$cit = $_POST['cit'];
	$str = $_POST['str'];
	$dob = $_POST['dob'];
	$des = $_POST['des'];
	$shi = $_POST['shi'];
	$sal = $_POST['sal'];
	$q = "insert into doctor values ('$doc_id','$nam','$dep_id','$cou','$sta','$cit','$str','$dob','des','shi','sal')";
	$a = $conn->query($q);
}

?>

<?php } ?>


<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<thead>
<tr>
    <th>Doctor ID</th>
    <th>Doctor Name</th>
    <th>Dept ID</th>
    <th>Country</th>
    <th>State</th>
    <th>City</th>
    <th>Street</th>
    <th>DOB</th>
    <th>Designation</th>
    <th>Shift</th>
    <th>Salary</th>
    <th>Action</th>
</tr>
</thead>
<tbody>
<?php
  $sql ="SELECT * FROM doctor";
  $qsql = mysqli_query($conn,$sql);
  while($rs = mysqli_fetch_array($qsql))
  {
    echo "<tr>
    <td>&nbsp;$rs[doctor_ID]</td>
    <td>&nbsp;$rs[name]</td>
    <td>&nbsp;$rs[department_ID]</td>
    <td>&nbsp;$rs[country]</td>
    <td>&nbsp;$rs[state]</td>
    <td>&nbsp;$rs[city]</td>
    <td>&nbsp;$rs[street]</td>
    <td>&nbsp;$rs[date_of_birth]</td>
    <td>&nbsp;$rs[designation]</td>
    <td>&nbsp;$rs[shift]</td>
    <td>&nbsp;$rs[salary]</td>
    <td>&nbsp;
    <a href='doctors.php?delid=$rs[doctor_ID]' class='btn btn-danger'>Delete</a> </td>
    </tr>";
  }        
?>
</tbody>


</table>
 <a href='doctors.php?add_id=$rs[doctor_ID]' class='btn btn-danger'>Add doctors</a> 
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>


