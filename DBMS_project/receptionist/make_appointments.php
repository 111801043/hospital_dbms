<?php
  session_start();
 include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Update Appointment Booking</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../receptionist.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Receptionist</a>
</li>
<li class="breadcrumb-item"><a href="make_appointments.php">Appointment Booking</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Patient ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="PID" placeholder="Enter patient ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Date of Appointment</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Date" placeholder="Enter date of appointment...." required="" >
        </div>

	<!--<label class="col-sm-2 col-form-label">Receptionist ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="RID" placeholder="Enter receptionist ID...." required="" >
        </div>
-->
	<label class="col-sm-2 col-form-label">Department</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Dept" placeholder="Enter department of appointment booking...." required="" >
        </div>

       
            </select>
        </div>

    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
  
 if(isset($_POST['btn_submit']))
 {
     $patient_id = $_POST['PID'];
     $apptmt_date = $_POST['Date'];
     $rid = $_SESSION['userid'];
     $dept = $_POST['Dept'];
     $call="CALL make_apptmt('$apptmt_date','$dept','$patient_id','$rid')";
     $result=$conn->query($call);
     
	
?>


<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">

<tbody>
  <?php
  while($rs = mysqli_fetch_array($result))
  {
  
    echo "<tr>
    <td>&nbsp;".$rs["booking_status"]."</td>
    </tr>";
  }        
?>
</tbody>


</table>
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>







<?php
}

$conn->close();

?>
