<?php include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Patient Registration</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../receptionist.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Receptionist</a>
</li>
<li class="breadcrumb-item"><a href="patient_reg.php">Patient Registration</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Name" placeholder="Enter name...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Country</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="country" placeholder="Enter country...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">State</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="state" placeholder="Enter state...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">City</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="city" placeholder="Enter city...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Street</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="street" placeholder="Enter street...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Gender</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="gender" placeholder="Enter gender...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Date of Birth</label>
        <div class="col-sm-4">
            <input type="date" class="form-control" name="DOB" placeholder="Enter DOB...." required="" >
        </div>

    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
if(isset($_POST['btn_submit'])){
		$query_new_id = mysqli_query($conn,"select concat('PA00',(select substring(patient_id,5,7)+1 from patient order by patient_ID desc limit 1)) as id");
		$res=mysqli_fetch_array($query_new_id);
		$pid=$res['id'];
		$name = $_POST['Name'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
        	$street = $_POST['street'];
        	$gender = $_POST['gender'];
        	$DOB = $_POST['DOB'];

		$sql = "INSERT INTO patient VALUES ('$pid',
			'$name','$country','$state','$city','$street','$gender','$DOB')";
		
		if(mysqli_query($conn, $sql)){
			echo "<h3 >Patient registration Successful</h3>";

			$sql2=mysqli_query($conn,"SELECT * from patient where patient_ID='$pid'");
			
		

?>

<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<thead>
  <tr>
    <th>Patient_ID</th>
    <th>Patient_Name</th>
    <th>Country</th>
    <th>State</th>
    <th>City</th>
    <th>street</th>
    <th>Gender</th>
    <th>Date_Of_Birth</th>
</tr>
</thead>
<tbody>
  <?php
  while($row = mysqli_fetch_array($sql2))
  {
  
    echo "<tr>
    <td>&nbsp;".$row["patient_ID"]."</td>
    <td>&nbsp;".$row["name"]."</td>
    <td>&nbsp;".$row["country"]."</td>
    <td>&nbsp;".$row["state"]."</td>
    <td>&nbsp;".$row["city"]."</td>
    <td>&nbsp;".$row["street"]."</td>
    <td>&nbsp;".$row["gender"]."</td>
    <td>&nbsp;".$row["date_of_birth"]."</td>
    </tr>";
    }
   
?>
</tbody>


</table>
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>




<?php
		}
		else{
			echo "ERROR: Sorry Registration failed $sql2. "
				. mysqli_error($conn);
		}
}
$conn->close();

?>