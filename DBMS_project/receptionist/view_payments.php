<?php include('head.php');?>
<?php include('connect.php');?>

<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>View Payments</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../receptionist.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Receptionist</a>
</li>
<li class="breadcrumb-item"><a href="view_payments.php">View Payments</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
if(isset($_GET['add_id']))
{
?>

<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<h6>Cleared Payments</h6>

<thead>
  <tr>
    <th>Payment_ID</th>
    <th>Receptionist_ID</th>
    <th>Patient_ID</th>
    <th>Appointment_ID</th>
    <th>Amount</th>
    <th>Date</th>
    <th>Mode_of_payment</th>
    <th>Payment_Status</th>
</tr>
</thead>
<tbody>
  <?php

    $query = "select * from payment where payment_status = 'PAID'";
    $qsql = $conn->query($query);
  while($rs = mysqli_fetch_array($qsql))
  {
  
    echo "<tr>
    <td>&nbsp;$rs[payment_ID]</td>
    <td>&nbsp;$rs[receptionist_ID]</td>
    <td>&nbsp;$rs[patient_ID]</td>
    <td>&nbsp;$rs[appointment_ID]</td>
    <td>&nbsp;$rs[amount]</td>
    <td>&nbsp;$rs[date]</td>
    <td>&nbsp;$rs[mode_of_payment]</td>
    <td>&nbsp;$rs[payment_status]</td>
    </tr>";
  }        
?>
</tbody>


</table>
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
}
?>

<div class="page-body">

<div class="card">
<div class="card-header">
    <div class="col-sm-10">
    </div>

</div>
<div class="card-block">
<div class="table-responsive dt-responsive">
<table id="dom-jqry" class="table table-striped table-bordered nowrap">
<h6>Pending Payments</h6>

<thead>
  <tr>
    <th>Payment_ID</th>
    <th>Receptionist_ID</th>
    <th>Patient_ID</th>
    <th>Appointment_ID</th>
    <th>Amount</th>
    <th>Date</th>
    <th>Mode_of_payment</th>
    <th>Payment_Status</th>
</tr>
</thead>
<tbody>
  <?php

    $query = "select * from payment where payment_status = 'NOT PAID'";
    $qsql = $conn->query($query);
  while($rs = mysqli_fetch_array($qsql))
  {
  
    echo "<tr>
    <td>&nbsp;$rs[payment_ID]</td>
    <td>&nbsp;$rs[receptionist_ID]</td>
    <td>&nbsp;$rs[patient_ID]</td>
    <td>&nbsp;$rs[appointment_ID]</td>
    <td>&nbsp;$rs[amount]</td>
    <td>&nbsp;$rs[date]</td>
    <td>&nbsp;$rs[mode_of_payment]</td>
    <td>&nbsp;$rs[payment_status]</td>
    </tr>";
  }        
?>
</tbody>


</table>
<a href='view_payments.php?add_id=$rs[payment_ID]' class='btn btn-danger'>View Cleared Payments</a> 
</div>
</div>
</div>







</div>

</div>
</div>

<div id="#">
</div>
</div>
</div>
</div>
</div>
</div>
</div>


