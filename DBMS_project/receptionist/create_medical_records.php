<?php include('head.php');?>
<?php include('connect.php');?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Create Medical Record</h4>

</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="../receptionist.php"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a>Receptionist</a>
</li>
<li class="breadcrumb-item"><a href="create_medical_records.php">Create Medical Record</a>
</li>
</ul>
<div class="navbar-wrapper">
<div class="navbar-container container-fluid">


<ul class="nav-right">

<li>
<a href="../index.php">
<i class="feather icon-log-out"></i> Logout
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<!-- <h5>Basic Inputs Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">

<form id="main" method="post" action="" enctype="multipart/form-data">
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Patient ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="patient-id" placeholder="Enter patient ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Receptionist ID</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="receptionist-id" placeholder="Enter receptionist ID...." required="" >
        </div>

	<label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-4">
            <textarea type="text" class="form-control" name="desc" rows="10" cols="30" required="" ></textarea>
        </div>


    </div>
    <div class="form-group row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" name="btn_submit" class="btn btn-primary m-b-0">Submit</button>
        </div>
    </div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
if(isset($_POST['btn_submit'])){
     $patient_id = $_POST['patient-id'];
     $rid = $_POST['receptionist-id'];
     $desc = $_POST['desc'];

     $gen_rno="select concat('RN0',(select substring(record_number,4,6)+1 from medical_record order by record_number desc limit 1))as rno";
     $res_rno=$conn->query($gen_rno);
     $rno=mysqli_fetch_array($res_rno);
     $rec_num=$rno['rno'];
     $ins="insert into medical_record values ('$rec_num','$patient_id','$desc')";
     $output=$conn->query($ins);
     
     $ins1="insert into maintains values ('$rec_num','$rid')";
     $output1=$conn->query($ins1);
}

$conn->close();

?>