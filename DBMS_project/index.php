<!DOCTYPE html>
<html lang = "eng">
	<head>
		<title>Hospital Database Management System</title>
		<meta charset = "utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel = "stylesheet" type = "text/css" href = "bootstrap.css" />
		<link rel = "stylesheet" type = "text/css" href = "customize.css" />
	</head>
<body>
	<div class = "navbar navbar-default navtop">
		<label class = "navbar-brand">Hospital Database Management System</label>
	</div>
		<div id = "sidelogin">
			<form action = "login.php" enctype = "multipart/form-data" method = "POST" >
				<label class = "lbllogin">Login Here</label>
				<br />
				<hr /style = "border:1px dotted #000;">
				<br />
				<div class="form-group">
				  <select name="username" class="form-control" required="required">
				    <option value="">-- Select One --</option>
				    <option value="admin">Admin</option>
				    <option value="doctor">Doctor</option>
				    <option value="patient">Patient</option>
				    <option value="nurse">Nurse</option>
				    <option value="receptionist">Receptionist</option>
				    <option value="tech_staff">Tech_staff</option>
				  </select>
				</div>
				<br />
				<div class = "form-group">
					<label for = "userid">UserID</label>
					<input class = "form-control" type = "text" name = "userid"  required = "required"/>
				</div>
				<br />
				<div class = "form-group">
					<label for = "password">Password</label>
					<input class = "form-control" type = "password" name = "password" required = "required" />
				</div>
				<br />
				<br />
				<div class = "form-group">
					<button class  = "btn btn-success form-control" type = "submit" value = "submit" name = "login" ><span class = "glyphicon glyphicon-log-in"></span> Login</button>
				</div>
			</form>
		</div>	
		<img src = "images/3.jpg" class = "background">			
</body>
</html>
