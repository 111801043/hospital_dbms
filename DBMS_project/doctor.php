<?php require_once('login_check.php');?>
<?php include('connect.php');?>

<!DOCTYPE html>
<html lang="english">
        <head>
        <title>Doctors Info</title>
        <link rel="stylesheet" type="text/css" href="doc.css">
        <body background = "https://image.freepik.com/free-photo/doctor-s-stethoscope-with-blue-background_23-2147652363.jpg">
</head>
    </head>
    <body>
        <h4> Welcome Doctor!! </h4>
        <div class = "doc">
           <ul>
                <li>
                    <a href="doc/doc_appointments.php"><div class="box">Appointments Info</div></a>
                </li>
		<li> 
                    <a href="doc/doc_appointment_status.php"><div class="box">Update Appointment Info</div></a>
                </li>
                <li> 
                    <a href="doc/view_medical_records.php"><div class="box">View Medical_Records</div></a>
                </li>
		<li> 
                    <a href="doc/update_medical_records.php"><div class="box">Update Medical_Records</div></a>
                </li> 
                <li> 
                    <a href="doc/doc_prescriptions.php"><div class="box">Prescriptions</div></a>
                </li>
                <li> 
                    <a href="doc/doc_tests.php"><div class="box">Tests</div></a>
                </li>
                <li> 
                    <a href="doc/doc_rooms.php"><div class="box">Room</div></a>
                </li>
           </ul>   
        </div>
    </body>
</html>