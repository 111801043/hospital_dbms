<?php require_once('login_check.php');?>
<?php include('connect.php');?>

<!DOCTYPE html>
<html lang="english">
        <head>
        <title>Patients Info</title>
        <link rel="stylesheet" type="text/css" href="doc.css">
        <body background = "https://image.freepik.com/free-photo/doctor-s-stethoscope-with-blue-background_23-2147652363.jpg">
</head>
    </head>
    <body>
        <h4> Welcome Patient!! </h4>
        <div class = "doc">
           <ul>
                <li>
                    <a href="patient/profile.php"><div class="box">Patient Profile</div></a>
                </li>
		<li> 
                    <a href="patient/bills.php"><div class="box">View Bills</div></a>
                </li>
                <li> 
                    <a href="patient/prescriptions.php"><div class="box">View Prescriptions</div></a>
                </li>    
                <li> 
                    <a href="patient/apt_status.php"><div class="box">View Appointment Status</div></a>
                </li>
                <li> 
                    <a href="patient/records.php"><div class="box">View Medical Records</div></a>
                </li>
                <li> 
                    <a href="patient/req_app.php"><div class="box">Request Appointment</div></a>
                </li>
           </ul>   
        </div>
    </body>
</html>