<?php require_once('login_check.php');?>
<?php include('connect.php');?>

<!DOCTYPE html>
<html lang="english">
        <head>
        <title>Admin Info</title>
        <link rel="stylesheet" type="text/css" href="doc.css">
        <body background = "https://image.freepik.com/free-photo/doctor-s-stethoscope-with-blue-background_23-2147652363.jpg">
</head>
    </head>
    <body>
        <h4> Welcome Admin!! </h4>
        <div class = "doc">
           <ul>
                
		<li> 
                    <a href="admin/patients.php"><div class="box">Patients</div></a>
                </li>
		<li> 
                    <a href="admin/doctors.php"><div class="box">Doctors</div></a>
                </li>
		<li> 
                    <a href="admin/payments.php"><div class="box">Payments</div></a>
                </li>
		<li> 
                    <a href="admin/medicines.php"><div class="box">Medicines</div></a>
                </li>
		<li> 
                    <a href="admin/tests.php"><div class="box">Tests</div></a>
                </li>
		<li> 
                    <a href="admin/tech_staff.php"><div class="box">Tech_staff</div></a>
                </li>
		<li> 
                    <a href="admin/nurses.php"><div class="box">Nurses</div></a>
                </li>
		<li> 
                    <a href="admin/receptionists.php"><div class="box">Receptionists</div></a>
                </li>
		<li> 
                    <a href="admin/rooms.php"><div class="box">Rooms</div></a>
                </li>
		<li> 
                    <a href="admin/appointments.php"><div class="box">Appointments</div></a>
                </li>
		<li> 
                    <a href="admin/medical_records.php"><div class="box">Medical Records</div></a>
                </li>
           </ul>   
        </div>
    </body>
</html>